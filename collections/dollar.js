const Mongoose = require('mongoose');

const Schema = Mongoose.Schema;

class Dollar {
    static getDollar() {
        return this.findOne();
    }
}

const DollarSchema = new Schema(
    {
        buy: {type: String},
        sell: {type: String}
    },
    {
        collection: 'dollar'
    }
);

DollarSchema.loadClass(Dollar);

module.exports = Mongoose.model('Dollar', DollarSchema);
