const Mongoose = require('mongoose');
const moment = require('moment-timezone');

const Schema = Mongoose.Schema;

class Recomendation {
    static getRecomendations() {
        let today = moment.tz('America/Argentina/Buenos_Aires').format();
        let yesterday = moment.tz('America/Argentina/Buenos_Aires').subtract(2, 'days');
        return this.find({
            date: {
                $gte: yesterday.toDate(),
                $lt: today
            }
        }).sort({date: -1});
    }

    static checkRecomendations(args) {
        let today = moment.tz(new Date(), 'America/Argentina/Buenos_Aires');
        let month = today.month().valueOf() + 1;
        if (month < 10) {
            month = '0' + month;
        }
        let date = today.year() + '-' + month + '-' + today.date();
        return this.countDocuments({
            date: {
                $eq: new Date(date)
            },
            id_user: args.id_user
        });
    }

    static saveRecomendation(payload) {
        return this(payload).save();
    }
}

const RecomendationSchema = new Schema(
    {
        name: {type: String},
        image: {type: String},
        recomendation: {type: String},
        date: {type: Date},
        id_user: {type: String},
        is_facebook: {type: String},
        is_google: {type: String}
    },
    {
        collection: 'recomendations'
    }
);

RecomendationSchema.loadClass(Recomendation);

module.exports = Mongoose.model('Recomendation', RecomendationSchema);
