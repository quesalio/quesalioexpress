const Mongoose = require('mongoose');
const axios = require('axios');
const Schema = Mongoose.Schema;

class Installation {
    static subscribeDraw(token) {
        let url = 'https://iid.googleapis.com/iid/v1/' + token + '/rel/topics/draws';
        return axios.post(url);
    }

    static subscribePrimera(token) {
        let url = 'https://iid.googleapis.com/iid/v1/' + token + '/rel/topics/primera';
        return axios.post(url);
    }

    static subscribeMatutina(token) {
        let url = 'https://iid.googleapis.com/iid/v1/' + token + '/rel/topics/matutina';
        return axios.post(url);
    }

    static subscribeVespertina(token) {
        let url = 'https://iid.googleapis.com/iid/v1/' + token + '/rel/topics/vespertina';
        return axios.post(url);
    }

    static subscribeNocturna(token) {
        let url = 'https://iid.googleapis.com/iid/v1/' + token + '/rel/topics/nocturna';
        return axios.post(url);
    }

    static saveInstallation(payload) {
        let url = "https://iid.googleapis.com/iid/info/" + payload.token + "?details=true";
        axios.defaults.headers.common['Authorization'] = "key=AAAAkEMVe6I:APA91bGo1YGBLUG1ZFqYGAggVm3fNCq6dMjAUfnkVBN4eGFWboj59dNBXHJE0S2WUAuXHkQziNguehacmlk-LnW5HVjBBBl8KvKYJ55zY8nbLTvqtfroi_0l0vHNvwIib25Kk3OCwmGo";
        return axios({
            method: 'get',
            url: url
        }).then((response) => {
            let topics = response.data.rel.topics;
            let topicsArray = [];
            for (let key in topics) {
                if (topics.hasOwnProperty(key)) {
                    if (key !== 'draws') {
                        topicsArray.push(key);
                    }
                }
            }
            return this.findOne({token: payload.token}).then((installation) => {
                if (installation) {
                    installation.updated_at = new Date();
                    installation.channels = topicsArray;
                    return installation.save();
                } else {
                    console.log('entra aqui en find');
                    return axios.all([this.subscribeDraw(payload.token), this.subscribePrimera(payload.token), this.subscribeMatutina(payload.token), this.subscribeVespertina(payload.token), this.subscribeNocturna(payload.token)])
                        .then(axios.spread(function (acct, perms) {
                            topicsArray = [];
                            topicsArray.push('primera');
                            topicsArray.push('matutina');
                            topicsArray.push('vespertina');
                            topicsArray.push('nocturna');
                            let Installation = Mongoose.model('Installation', InstallationSchema);
                            let install = new Installation();
                            install.token = payload.token;
                            install.channels = topicsArray;
                            return install.save();
                        }));
                }
            }).catch((err) => {
                /*let Installation = Mongoose.model('Installation', InstallationSchema);
                let install = new Installation();
                install.token = payload.token;
                install.channels = topicsArray;
                return install.save();*/
            });
        }).catch((err) => {
            console.log('entra aqui en nuevo');
            return axios.all([this.subscribeDraw(payload.token), this.subscribePrimera(payload.token), this.subscribeMatutina(payload.token), this.subscribeVespertina(payload.token), this.subscribeNocturna(payload.token)])
                .then(axios.spread(function (acct, perms) {
                    let topicsArray = [];
                    topicsArray.push('primera');
                    topicsArray.push('matutina');
                    topicsArray.push('vespertina');
                    topicsArray.push('nocturna');
                    let Installation = Mongoose.model('Installation', InstallationSchema);
                    let install = new Installation();
                    install.token = payload.token;
                    install.channels = topicsArray;
                    return install.save();
                })).catch(erro => {
                    console.log(erro);
                });
        });

        //return this(payload).save();
    }

    static updateChannels(payload) {
        let channels = payload.channels;
        if (channels.length > 0) {
            channels = channels.split(',');
        } else {
            channels = [];
        }
        return this.findOne({token: payload.token})
            .then((installation) => {
                installation.channels = channels;
                return installation.save();
            })
            .catch(err => {

            });
    }
}

const InstallationSchema = new Schema({
        token: {type: String},
        channels: [{type: String}],
        created_at: {type: Date, default: new Date()},
        updated_at: {type: Date, default: new Date()}
    },
    {
        collection: 'installation'
    });

InstallationSchema.loadClass(Installation);
module.exports = Mongoose.model('Installation', InstallationSchema);
