const MeaningType = require('../collections/meaning_type');

const getMeaningTypes = async payload => {
    return await MeaningType.getMeaningTypes();
};

const getMeaningType = async payload => {
    return await MeaningType.getMeaningType(payload.id);
};
module.exports.getMeaningTypes = getMeaningTypes;
module.exports.getMeaningType = getMeaningType;