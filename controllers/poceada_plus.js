const PoceadaPlus = require('../collections/poceada_plus');

const getPoceadaPlus = async payload => {
    return await PoceadaPlus.getPoceadaPlus();
};
const getPoceadaPlusLimit = async payload => {
    return await PoceadaPlus.getPoceadaPlusLimit(payload.limit);
};
module.exports.getPoceadaPlus = getPoceadaPlus;
module.exports.getPoceadaPlusLimit = getPoceadaPlusLimit;
