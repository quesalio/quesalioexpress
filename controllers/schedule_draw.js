const ScheduleDraw = require('./../collections/schedule_draw');

const getScheduleDraw = async (root, payload, context) => {
    return await ScheduleDraw.getScheduleDraw(payload.draw, payload.day);
};
module.exports = getScheduleDraw;