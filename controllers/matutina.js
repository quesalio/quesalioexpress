const Matutina = require('../collections/matutina');

const getMatutina = async (root, payload, context) => {
    return await Matutina.getMatutina(payload.ids, payload.date);
};


const getMatutinaModified = async (root, payload, context) => {
    return await Matutina.getMatutinaModified(payload.ids);
};
module.exports.getMatutina = getMatutina;
module.exports.getMatutinaModified = getMatutinaModified;
