const Vespertina = require('../collections/vespertina');

const getVespertina = async (root, payload, context) => {
    return await Vespertina.getVespertina(payload.ids, payload.date);
};
const getVespertinaModified = async (root, payload, context) => {

    return await Vespertina.getVespertinaModified(payload.ids);
};
module.exports.getVespertina = getVespertina;
module.exports.getVespertinaModified = getVespertinaModified;
