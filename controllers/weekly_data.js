const WeeklyData = require('./../collections/weekly_data');

const getWeeklyData = async payload => {
    return await WeeklyData.getWeeklyData();
};
module.exports = getWeeklyData;