const Installation = require('./../collections/installation');

const saveInstallation = async payload => {
    //console.log(payload);
    return await Installation.saveInstallation(payload);
};
const updateChannels = async payload => {
    return await Installation.updateChannels(payload);
};

//module.exports.getRecomendations = getRecomendations;
module.exports.updateChannels = updateChannels;
module.exports.saveInstallation = saveInstallation;