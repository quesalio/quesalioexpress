const Recomendation = require('./../collections/recomendation');

const getRecomendations = async payload => {
    return await Recomendation.getRecomendations();
};
const checkRecomendations = async payload => {
    return await Recomendation.checkRecomendations(payload);
};
const saveRecomendation = async payload => {

    return await Recomendation.saveRecomendation(payload);
};
module.exports.getRecomendations = getRecomendations;
module.exports.checkRecomendations = checkRecomendations;
module.exports.saveRecomendation = saveRecomendation;