const Dollar = require('./../collections/dollar');

const getDollar = async payload => {
    return await Dollar.getDollar();
};
module.exports = getDollar;