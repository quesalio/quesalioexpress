const Loto = require('../collections/loto');

const getLoto = async payload => {
    return await Loto.getLoto();
};
const getLotoLimit = async payload => {
    return await Loto.getLotoLimit(payload.limit);
};

module.exports.getLoto = getLoto;
module.exports.getLotoLimit = getLotoLimit;
