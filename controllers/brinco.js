const Brinco = require('./../collections/brinco');

const getBrinco = async payload => {
    return await Brinco.getBrinco();
};
const getBrincoLimit = async payload => {
    return await Brinco.getBrincoLimit(payload.limit);
};
module.exports.getBrincoLimit = getBrincoLimit;
module.exports.getBrinco = getBrinco;