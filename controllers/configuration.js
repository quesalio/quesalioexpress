const Configuration = require('./../collections/configuration');

const getConfiguration = async (root, payload, context) => {
    try {
        return await Configuration.getConfiguration();
    } catch (error) {
        console.log(error.message);
    }
};
module.exports = getConfiguration;