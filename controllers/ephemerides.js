const Ephemerides = require('./../collections/ephemerides');

const getEphemerides = async payload => {
    return await Ephemerides.getEphemerides();
};
module.exports = getEphemerides;