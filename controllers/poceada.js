const Poceada = require('./../collections/poceada');

const getPoceada = async payload => {
    return await Poceada.getPoceada();
};
const getPoceadaLimit = async payload => {
    return await Poceada.getPoceadaLimit(payload.limit);
};

module.exports.getPoceada = getPoceada;
module.exports.getPoceadaLimit = getPoceadaLimit;
