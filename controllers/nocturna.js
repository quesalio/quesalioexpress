const Nocturna = require('../collections/nocturna');

const getNocturna = async (root, payload, context) => {
    return await Nocturna.getNocturna(payload.ids, payload.date);
};
const getNocturnaModified = async (root, payload, context) => {
    return await Nocturna.getNocturnaModified(payload.ids);
};

module.exports.getNocturna = getNocturna;
module.exports.getNocturnaModified = getNocturnaModified;
