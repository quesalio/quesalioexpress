const Primera = require('../collections/primera');

const getPrimera = async (root, payload, context) => {
    return await Primera.getPrimera(payload.ids, payload.date);
};
const getPrimeraModified = async (root, payload, context) => {
    return await Primera.getPrimeraModified(payload.ids);
};
module.exports.getPrimera = getPrimera;
module.exports.getPrimeraModified = getPrimeraModified;
