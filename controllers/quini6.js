const Quini6 = require('../collections/quini6');

const getQuini6 = async payload => {
    return await Quini6.getQuini6();
};
const getQuini6Limit = async payload => {
    return await Quini6.getQuini6Limit(payload.limit);
};
module.exports.getQuini6 = getQuini6;
module.exports.getQuini6Limit = getQuini6Limit;
