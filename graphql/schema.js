const path = require('path');
const {fileLoader, mergeTypes} = require('merge-graphql-schemas');
const typesArray = fileLoader(path.join(__dirname, './schemas'));
const SCHEMAS = mergeTypes(typesArray, {all: true});
module.exports = SCHEMAS;