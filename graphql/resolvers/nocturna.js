const {getNocturna} = require('../../controllers/nocturna');
const {getNocturnaModified} = require('../../controllers/nocturna');

const {PubSub} = require('apollo-server');
const pubsub = new PubSub();


const NOCTURNA_MODIFIED = 'nocturna_modified';

const nocturna = {
    Query: {
        nocturna(root, payload, context) {
            return getNocturna(root, payload, context);
        },
        nocturna_modified(root, payload, context) {
            let results = getNocturnaModified(root, payload, context);
            pubsub.publish(NOCTURNA_MODIFIED, results);
            return results;
        }
    },
    Subscription: {
        nocturna_modified: {
            resolve: (payload, args, context, info) => {
                return payload;
            },
            subscribe: () => pubsub.asyncIterator(NOCTURNA_MODIFIED)
        }
    }
};
module.exports = nocturna;