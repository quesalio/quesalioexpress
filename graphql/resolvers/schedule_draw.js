const getScheduleDraw = require('../../controllers/schedule_draw');

const schedule_draw = {
    Query: {
        schedule_draw(root, payload, context) {
            return getScheduleDraw(root, payload, context);
        }
    }
};
module.exports = schedule_draw;