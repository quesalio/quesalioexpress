const {getPrimera} = require('../../controllers/primera');
const {getPrimeraModified} = require('../../controllers/primera');

const {PubSub} = require('apollo-server');
const pubsub = new PubSub();


const PRIMERA_MODIFIED = 'primera_modified';

const primera = {
    Query: {
        primera(root, payload, context) {
            return getPrimera(root, payload, context);
        },
        primera_modified(root, payload, context) {
            let results = getPrimeraModified(root, payload, context);
            pubsub.publish(PRIMERA_MODIFIED, results);
            return results;
        }
    },
    Subscription: {
        primera_modified: {
            resolve: (payload, args, context, info) => {
                return payload;
            },
            subscribe: () => pubsub.asyncIterator(PRIMERA_MODIFIED)
        }
    }
};
module.exports = primera;