const getConfigurationData = require('./../../controllers/configuration');

const {PubSub} = require('apollo-server');
const pubsub = new PubSub();

const CONFIGURATION_CHANGED = 'configuration_changed';

const configuration = {
    Query: {
        configuration(root, payload, context) {
            return getConfigurationData();
        },
        configuration_changed(root, payload, context) {
            let results = getConfigurationData();
            pubsub.publish(CONFIGURATION_CHANGED, results);
            return results;
        }
    },
    Subscription: {
        configuration_changed: {
            subscribe: () => pubsub.asyncIterator(CONFIGURATION_CHANGED)
        }
    }
};
module.exports = configuration;
