const {getRecomendations} = require('./../../controllers/recomendation');
const {checkRecomendations} = require('./../../controllers/recomendation');
const {saveRecomendation} = require('./../../controllers/recomendation');

const recomendation = {
    Query: {
        recomendation(root, payload, context) {
            return getRecomendations();
        },
        check_recomendations(root, payload, context) {
            return checkRecomendations(payload);
        }
    },
    Mutation: {
        save_recomendation(root, payload, context) {
            return saveRecomendation(payload);
        }
    }
};
module.exports = recomendation;
