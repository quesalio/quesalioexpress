const {getMeaningType} = require('./../../controllers/meaning_type');
const {getMeaningTypes} = require('./../../controllers/meaning_type');

const meaning_type = {
    Query: {
        meaning_types(root, payload, context) {
            return getMeaningTypes();
        },
        meaning_type(root, payload, context) {
            return getMeaningType(payload);
        }
    }
};
module.exports = meaning_type;