const getDollar = require('./../../controllers/dollar');

const dollar = {
    Query: {
        dollar(root, payload, context) {
            return getDollar();
        }
    }
};
module.exports = dollar;
