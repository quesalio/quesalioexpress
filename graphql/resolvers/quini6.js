const {getQuini6Limit} = require('./../../controllers/quini6');
const {getQuini6} = require('./../../controllers/quini6');

const quini6 = {
    Query: {
        quini6(root, payload, context) {
            return getQuini6(root, payload, context);
        },
        quini6_limit(root, payload, context) {
            return getQuini6Limit( payload);
        }
    }
};

module.exports = quini6;
