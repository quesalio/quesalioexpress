//const {getRecomendations} = require('./../../controllers/recomendation');
//const {checkRecomendations} = require('./../../controllers/recomendation');
const {saveInstallation, updateChannels} = require('./../../controllers/installation');

const installation = {
    /*Query: {
        recomendation(root, payload, context) {
            return getRecomendations();
        },
        check_recomendations(root, payload, context) {
            return checkRecomendations(payload);
        }
    },*/
    Mutation: {
        save_installation(root, payload, context) {
            return saveInstallation(payload);
        },
        update_channels(root, payload, context) {
            return updateChannels(payload);
        }
    }
};
module.exports = installation;
