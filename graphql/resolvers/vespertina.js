const {getVespertinaModified} = require('../../controllers/vespertina');
const {getVespertina} = require('../../controllers/vespertina');

const {PubSub} = require('apollo-server');
const pubsub = new PubSub();


const VESPERTINA_MODIFIED = 'vespertina_modified';

const vespertina = {
    Query: {
        vespertina(root, payload, context) {
            return getVespertina(root, payload, context);
        },
        vespertina_modified(root, payload, context) {
            let results = getVespertinaModified(root, payload, context);
            pubsub.publish(VESPERTINA_MODIFIED, results);
            return results;
        }
    },
    Subscription: {
        vespertina_modified: {
            resolve: (payload, args, context, info) => {
                return payload;
            },
            subscribe: () => pubsub.asyncIterator(VESPERTINA_MODIFIED)
        }
    }
};
module.exports = vespertina;