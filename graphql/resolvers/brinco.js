const {getBrinco} = require('./../../controllers/brinco');
const {getBrincoLimit} = require('./../../controllers/brinco');

const brinco = {
    Query: {
        brinco(root, payload, context) {
            return getBrinco(root, payload, context);
        },
        brinco_limit(root, payload, context) {
            return getBrincoLimit(payload);
        }
    }
};
module.exports = brinco;
