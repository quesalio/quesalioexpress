const getWeeklyData = require('./../../controllers/weekly_data');


const weekly_data = {
    Query: {
        weekly(root, payload, context) {
            return getWeeklyData();
        }
    }
};

module.exports = weekly_data;
