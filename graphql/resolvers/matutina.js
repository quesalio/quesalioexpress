const {getMatutina} = require('../../controllers/matutina');
const {getMatutinaModified} = require('../../controllers/matutina');

const {PubSub} = require('apollo-server');
const pubsub = new PubSub();


const MATUTINA_MODIFIED = 'matutina_modified';

const matutina = {
    Query: {
        matutina(root, payload, context) {
            return getMatutina(root, payload, context);
        },
        matutina_modified(root, payload, context) {
            let results = getMatutinaModified(root, payload, context);
            pubsub.publish(MATUTINA_MODIFIED, results);
            return results;
        }
    },
    Subscription: {
        matutina_modified: {
            resolve: (payload, args, context, info) => {
                return payload;
            },
            subscribe: () => pubsub.asyncIterator(MATUTINA_MODIFIED)
        }
    }
};
module.exports = matutina;