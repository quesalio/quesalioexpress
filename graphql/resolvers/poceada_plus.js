const {getPoceadaPlusLimit} = require('./../../controllers/poceada_plus');
const {getPoceadaPlus} = require('./../../controllers/poceada_plus');

const poceada_plus = {
    Query: {
        poceada_plus(root, payload, context) {
            return getPoceadaPlus(root, payload, context);
        },
        poceada_plus_limit(root, payload, context) {
            return getPoceadaPlusLimit(payload);
        }
    }
};

module.exports = poceada_plus;
