const getEphemerides = require('./../../controllers/ephemerides');

const ephemerides = {
    Query: {
        ephemerides(root, payload, context) {
            return getEphemerides();
        }
    }
};
module.exports = ephemerides;