const {getLoto} = require('./../../controllers/loto');
const {getLotoLimit} = require('./../../controllers/loto');

const loto = {
    Query: {
        loto(root, payload, context) {
            return getLoto(root, payload, context);
        },
        loto_limit(root, payload, context) {
            return getLotoLimit(payload);
        }
    }
};

module.exports = loto;
