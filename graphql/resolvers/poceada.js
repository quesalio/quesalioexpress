const {getPoceadaLimit} = require('./../../controllers/poceada');
const {getPoceada} = require('./../../controllers/poceada');

const poceada = {
    Query: {
        poceada(root, payload, context) {
            return getPoceada(root, payload, context);
        },
        poceada_limit(root,payload,context) {
            return getPoceadaLimit(payload);
        }
    }
};

module.exports = poceada;
