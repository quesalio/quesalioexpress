const express = require('express');
const mongoose = require('mongoose');
const fs = require("fs");
const {ApolloEngine} = require('apollo-engine');
const {ApolloServer} = require('apollo-server-express');
const SCHEMAS = require('./graphql/schema');
const RESOLVERS = require('./graphql/resolvers');
const {InMemoryCache} = require('apollo-cache-inmemory');

const cache = new InMemoryCache();
const http = require('http');
const https = require('https');
require('dotenv').config();
const configurations = {
    // Note: You may need sudo to run on port 443
    production: {ssl: true, port: 443, hostname: 'production.quesalio.com'},
    //development: {ssl: false, port: 4000, hostname: 'production.quesalio.com'}
    development: {ssl: false, port: 4000, hostname: 'localhost'}
};
const environment = process.env.NODE_ENV || 'production';
const config = configurations[environment];

mongoose.connect('mongodb://quesalio:quesalio777@51.68.133.225/quesalio', {useNewUrlParser: true});
let db = mongoose.connection;
db.on('error', () => {
    console.log('--Failed to connect to mongose')
});
db.once('open', () => {
    console.log('connected to mongoose');
});
const app = express();
//const httpServer = http.createServer(app);

let server;
if (config.ssl) {
    // Assumes certificates are in .ssl folder from package root. Make sure the files
    // are secured.
    server = https.createServer(
        {
            key: fs.readFileSync(`/etc/letsencrypt/live/production.quesalio.com/privkey.pem`),
            cert: fs.readFileSync(`/etc/letsencrypt/live/production.quesalio.com/fullchain.pem`)
        },
        app
    );
} else {
    server = http.createServer(app);
}
let url_endpoint;
if (config.ssl) {
    url_endpoint = `https://${config.hostname}/graphql`;
} else {
    url_endpoint = `http://${config.hostname}:${config.port}/graphql`;
}
const apollo = new ApolloServer({
    typeDefs: SCHEMAS,
    resolvers: RESOLVERS,
    playground: {
        endpoint: url_endpoint,
        settings: {
            'editor.theme': 'light'
        }
    },
    introspection: true,
    tracing: true,
    cacheControl: true,
    cache
});

const engine = new ApolloEngine({
    apiKey: 'service:puerquitoyuki-2476:-wdA6rzEOTd-YDiMyRqKvA'
});

apollo.applyMiddleware({app});
apollo.installSubscriptionHandlers(server);
//app.listen({port: 4000}, () => console.log('Express GraphQL Server Now Running On localhost:4000/graphql'));

server.listen(config.port, () => {
    console.log(`🚀 Server ready at http://${config.hostname}:${config.port}${apollo.graphqlPath}`);
    console.log(`🚀 Subscriptions ready at ws://${config.hostname}:${config.port}${apollo.subscriptionsPath}`)
});

//para el engine
/*engine.listen({
    port: config.port,
    graphqlPaths: ['/graphql'],
    httpServer: server
}, () => {
    console.log('Listening!');
});*/